package $ReferenceClasses;

import sun.plugin.dom.css.Counter;

import java.lang.ref.WeakReference;

public class _1App {
    public static void main(String[] args) {
        String s = new String(); // strong reference
        WeakReference weakCounter = new WeakReference(s); //weak reference
        s = null; // now Counter object is eligible for garbage collection
    }
}
