package _1basics.arrays;

import java.util.ArrayList;
import java.util.List;

/*
ковариантность
 */
public class _1Covariation {
    public static void main(String[] args) {
        Object obj= new float[5];
        byte[] myByte= new byte[5];
        obj=myByte;
        float[] myFloat = new float[5];
        myFloat = (float[]) obj;/*
                                      Exception in thread "main" java.lang.ClassCastException: [B cannot be cast to [F
	                            at _1basics.arrays._1Covariation.main(_1Covariation.java:9)
	                            */
        List<Integer>  li= new ArrayList<Integer>();
        List<Number> ln= new ArrayList<Number>();
//        ln=li;
        ln.add(new Float(12.3f));

    }
}
