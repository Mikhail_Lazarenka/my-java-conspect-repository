package $Reflection;

import java.lang.reflect.Field;
import java.lang.reflect.Method;

/**
 * https://www.concretepage.com/java/how-to-access-all-private-fields-methods-and-constructors-using-java-reflection-with-example
 */

public class MyReflection {
    public static void main(String[] args) throws NoSuchFieldException, IllegalAccessException {
        MyClass myClass = new MyClass(10);

        Field fieldI=MyClass.class.getDeclaredField("i");
        fieldI.setAccessible(true);//java.lang.IllegalAccessException
        System.out.println(fieldI.getInt(myClass));

    }
}
